//package javascriptParser;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.regex.*;
import java.util.Vector;

import javascriptParser.*;

public class Main {

	public static String INPUT_PATH = "./Input/";

	private static HashMap<String,String> tenField = new HashMap<String, String>();

	private static String[] listThanhPho = { "ha-noi", "ho-chi-minh", "da-nang", "can-tho", "khanh-hoa","vung-tau","hai-phong","nam-dinh","tien-giang","dong-nai","binh-duong","long-an","tra-vinh",
		"an-giang","bac-lieu","dong-thap","vinh-long","ben-tre","ca-mau","hau-giang","binh-phuoc","kien-giang","soc-trang","tay-ninh","quang-ninh","lao-cai","thanh-hoa","ha-giang",
		"ha-nam","son-la","thai-binh","bac-giang","dien-bien","lang-son","phu-tho","thai-nguyen","vinh-phuc","bac-kan","bac-ninh","cao-bang","hai-duong","lai-chau","lam-dong","binh-thuan",
		"kon-tum","hue","quang-nam","nghe-an","ninh-thuan","gia-lai","phu-yen","dak-lak","ha-tinh","binh-dinh","quang-ngai","quang-tri","quang-binh","dak-nong"
	};

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{

		String outputDirectory = ReadConfigFile.getInstance().getConfigValue("OutputPath");
		System.out.println("output = " + outputDirectory);
		//String fileInput = ReadConfigFile.getInstance().getConfigValue("InputPath"); 
		int startingPage = Integer.parseInt(ReadConfigFile.getInstance().getConfigValue("StartingPage")); 
		int endingPage = Integer.parseInt(ReadConfigFile.getInstance().getConfigValue("EndingPage"));
		String seedListFile = ReadConfigFile.getInstance().getConfigValue("SeedList");
		String pageParameter = ReadConfigFile.getInstance().getConfigValue("PageParameter");
		String staticRequestParams = ReadConfigFile.getInstance().getConfigValue("RequestParameters");
		RequestParams requestParams = new RequestParams(staticRequestParams);
		//staticRequestParams = staticRequestParams.replaceAll("@#", "&");
		String seedListContent = "";

		//init(fileInput);

		// Regex used to grep all content links
		String regex = "result-image\">[ ]*<a href=\"(/[a-z-]*/[a-z0-9-]*)\"";

		// JavaScript Parser - Send Post request                
		for (int j = 0; j< listThanhPho.length; j++){
			String thanhPho = listThanhPho[j];
			String endPoint = ReadConfigFile.getInstance().getConfigValue("EndPointURL").replaceAll("@#", thanhPho);
			System.out.println("URL = " + endPoint);
			for (int i = startingPage ; i <= endingPage; i++ ){
				String requestParameters = requestParams.toUrlParams() + "&" + pageParameter + "="+i;
				String fileOutput = outputDirectory + ReadConfigFile.getInstance().getConfigValue("OutputFileName") + 
										"_" + thanhPho + "_" + i + ".html";
				String response = HttpRequestPoster.sendPostRequest(endPoint, requestParameters);
				// Test purpose - TO REMOVE
				HttpRequestPoster.writeToFile(fileOutput, response);

				// Read local file to grep content links
				String contentLinks =grepContentLink(response, "http://www.foody.vn", regex);
				if (contentLinks.equals("")){
					System.out.println("Last page:" + fileOutput);
					break;
				}
				seedListContent += contentLinks;
				//System.out.println("Xong trang " + i + " - " + ReadConfigFile.getInstance().getConfigValue("OutputFileName"));
				System.out.println("Xong trang " + fileOutput);
			}
		}
		// write all links to seedList
		HttpRequestPoster.writeToFile(seedListFile, seedListContent);

		/*
		// Call JSONParser
		for (int i = startingPage ; i <= endingPage; i++){
		String fileOutput = outputDirectory +"DiaDiem_" + i + ".html";
		JSONParser myJSonParser = new JSONParser(fileOutput,tenField);
		myJSonParser.decode();
		}
		 */
	}

	/**
	 * Grep all content links from a page content, with specific regex
	 * @param pageContent: listing page content
	 * @param prefix: prefix to add before each content link
	 * @param regex: regex to grep content link
	 * @return: a string which contents all content links
	 */
	private static String grepContentLink(String pageContent, String prefix, String regex){
		StringBuffer contentLinks = new StringBuffer();
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(pageContent);

		while(m.find()){
			contentLinks.append(prefix + m.group(1) + "\n");
		}
		return contentLinks.toString();
	}



	private static void init(String fileInput) throws Exception{
		// Create HashMap TenField (used to JSONParser)
		// Read .csv file line by line (csv file contains the input for JSONParser)
		File fileCSV = new File(fileInput);
		Vector<String> vTmp = new Vector<String>(); // Vector contains all lines from input file
		FileInputStream fstream = new FileInputStream(fileCSV);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		while( (strLine = br.readLine()) != null) {
			strLine = strLine.replaceAll("[|]{2,}", "");
			strLine = strLine.replace("|", "@#");
			vTmp.add(strLine);
		}
		in.close();

		// Fill HashMap tenField 
		String[] fieldName = vTmp.get(0).split("@#"); // fieldName could be sth like: Ten, DiaChi, DienThoai ....
		String[] fieldNameMongo = vTmp.get(1).split("@#"); // fieldName is the name in the JSON file
		for(int i=0; i<fieldName.length; i++){
			tenField.put(fieldName[i], fieldNameMongo[i]);
		}
	}

}
