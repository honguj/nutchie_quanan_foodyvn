#!/bin/bash

# nutch directory
NUTCH_HOME="/all_nutch/nutchie_quanan_foodyvn/nutch"
ngay=`date`
mkdir -p "backUp/$ngay"
dbName="restaurantFoodyvn"
backupFolder="$NUTCH_HOME/backUp/$ngay"

cd $NUTCH_HOME

#http_proxy=http://192.168.1.2:12346
#export http_proxy

cd $NUTCH_HOME/tqa-ListingFinder
make run
cp seedList.txt ./../urls/seedList.txt

cd $NUTCH_HOME
# start crawling
echo "STARTING CRAWL PROCESS"
./fullFetch.sh
echo "FINISH CRAWL PROCESS"

# Push new data to service 16
#cd $NUTCH_HOME/NewDataSender/src
#make run
cd $NUTCH_HOME

# backup data
echo "BACKUP PROCESS"
echo $backupFolder
cd "$backupFolder"
mkdir nutchdb
mkdir seed
cd $NUTCH_HOME

#backup mongodb
backupFileName=$dbName".json"
mongoexport -d $dbName -c Quanan -o "$backupFolder/$backupFileName"

#backup nutch db
cp -r ./crawl/crawldb/current "$backupFolder/nutchdb"

#backup seedlist.txt
cp $NUTCH_HOME/urls/seedList.txt "$backupFolder/seed"

# zip the backup
echo "COMPRESSING DATA"
cd "$backupFolder"

7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on mongoDb.7z $backupFilename
rm $backupFileName

7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on nutchdb.7z nutchdb
rm -r nutchdb

7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on seed.7z seed
rm -r seed

cd $NUTCH_HOME
echo "COMPRESSING DATA DONE"
echo "BACKUP DONE"

#unset http_proxy
