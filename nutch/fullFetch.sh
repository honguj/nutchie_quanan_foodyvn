#!/bin/bash

#############################################################################################
#Name: fullFetch.sh
#Author: Duc Nguyen
#Date: 11 Jul 2011
#Description: make a full cycle of crawling
#	1) injecting url from urls/seed.txt to crawldb
#	2) Generate a new segment to crawl form crawldb
#	3) Fetch all the link from the segment
#	4) Parse all fetched links with plugin
#	5) Update crawldb with news links fetched
#############################################################################################

# short script to help crawl
#export JAVA_HOME=/usr/lib/jvm/java-6-openjdk-amd64/
 
# set this to your nutch home dir
NUTCH_HOME="/all_nutch/nutchie_quanan_foodyvn/nutch"
BIN=$NUTCH_HOME/runtime/local/

cd $NUTCH_HOME
# inject the seed urls file to the crawldb
#${NUTCH_HOME}/bin/nutch inject crawl/crawldb urls

# repeat the crawl processus many times (depth)
for i in $(seq 1 1)
do
	echo "LAN "$i
	
	# inject the seed urls file to the crawldb (inject tu lastPages.txt , listingPage.txt, resto-seed.txt)
	$BIN/bin/nutch inject crawl/crawldb urls	
	echo "Inject done"

	# generate new fetchList from the crawldb (can change the topN)
	$BIN/bin/nutch generate crawl/crawldb crawl/segments -topN 10000

	unset SEGMENT
	# pick the lastest segment on the crawldb
	export SEGMENT=crawl/segments/`ls -tr crawl/segments|tail -1`
	echo $SEGMENT

	# launch the crawleri
	$BIN/bin/nutch fetch $SEGMENT -noParsing 
	echo "fetch done"

	# parse the fetched page (possibility to separate fetch processus and parse processus with option "-noParsing" in last step)
	$BIN/bin/nutch parse $SEGMENT
	echo "parse done"

	# update the crawldb
	$BIN/bin/nutch updatedb crawl/crawldb $SEGMENT

	echo "LAN "$i" XONG"
	echo " "	

done 

# success!
exit 0

#EOF
