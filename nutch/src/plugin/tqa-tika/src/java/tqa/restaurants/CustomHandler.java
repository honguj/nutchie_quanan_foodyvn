package tqa.restaurants;

// JDK imports
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Logger;
import java.io.*;
import java.util.*;
import java.text.*;
import java.security.*;
import java.lang.*;

import java.io.ByteArrayInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//Tika imports
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.apache.tika.exception.TikaException;

/**
 * Handler used to parse link content
 * @author: Hoang Tay Duc Nguyen
 */
public class CustomHandler implements ContentHandler {
	/**
	 * location where we use to store image in link content
	 */
	public static final String SOURCE_PATH_IMAGE = "/all_nutch/Img/";
	// class members
	private StringBuffer _content= new StringBuffer();
	private String[] _startContent;
	private String[] _endContent;
	private boolean _getContent = false;
	private Vector _imgLink;
	
	/**
	 * Construstor: 
	 * 		-create a link content from an URL link
	 *		-create a table _imgLink to contains all img link in the html page 
	 * @param link : URL to parse
	 */
	public CustomHandler(String link){
		System.out.println("My custom Handler - link = " + link);
	}
	
	/**
	 * Function called for open tag
	 * @param uri
	 * @param qName : tag name
	 * @param attributes : list of all attributes in open tag
	 */
	public void startElement(String uri, String localName, String qName, Attributes attributes)
	throws SAXException {
		if (qName.equals("meta")){
			System.out.println("found meta");
			for (int i = 0 ; i < attributes.getLength(); i++){
				System.out.println(attributes.getQName(i) + " - " + attributes.getValue(i));		
			}
		}
	}
	
	/**
	 * Function called for tag's content
	 * @param ch[] table of all characters in tag's content
	 *  
	 */
	public void characters(char ch[], int start, int length) throws SAXException {
		String s = new String (ch,start,length);

	}

	/**
	 * Function called when close tag
	 * @param qName : tag's name
	 */
	public void endElement(String uri, String localName, String qName)
	throws SAXException {
	}
	
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void endPrefixMapping(String arg0) throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void ignorableWhitespace(char[] arg0, int arg1, int arg2)
			throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void processingInstruction(String arg0, String arg1)
			throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void setDocumentLocator(Locator arg0) {
		// TODO Auto-generated method stub
		
	}

	public void skippedEntity(String arg0) throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub
		
	}

	public void startPrefixMapping(String arg0, String arg1)
			throws SAXException {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Split a string s by "@#" and put in table
	 * @param s
	 * @return 
	 */
	public String[] fillTable(String s){
		// String dc chia cat boi chuoi "!@#"
		String[] table = s.split("@#");
		return table;
	}
	
	/**
	 * Split a string s by "@#", put in table and convert all elements in the table to Integer
	 * @param s
	 * @return
	 */
	public int[] fillTableInt(String s){
		String[] tmp = s.split("@#");
		for (int i =0; i<tmp.length; i++){
			if (tmp[i].contains("!")){
				String[] tmp2 = tmp[i].split("!");
				tmp[i] = tmp2[0];
				//_endCode3 = Integer.parseInt(tmp2[1]);
			}	
		}
		int[] table = new int[tmp.length];
		for(int i=0; i<tmp.length; i++){
			table[i] = Integer.parseInt(tmp[i]);
             }
		return table; 
	}
	
	/**
	 * Function to get the data from data base and put in some tables to parse content
	 */
/*	private boolean init(){
		boolean success = true;
		try {
			String startContent = ReadConfigFile.getInstance().getConfigValue("StartContent");
			String endContent = ReadConfigFile.getInstance().getConfigValue("EndContent");
			_startContent = fillTable(startContent); 
			_endContent = fillTable(endContent);
		} catch (Exception e) {
			success = false;
			System.err.println("Error in init() ListingHandler class: " + e.getMessage());
		}
		return success;
	}
*/	
	/**
	 * Functions to detect if a list of attributes contains an attributeName specified
	 * @param attributes
	 * @param attributeName
	 * @return
	 */
	private boolean containsAttribute(Attributes attributes, String attributeName){
		boolean contains = false;
		for (int i = 0; i<attributes.getLength(); i++){
			String name = attributes.getQName(i);
			//System.out.print(name + " - ");
			if (name.equals(attributeName)){
				contains = true;
				break;
			}
		}
		return contains;
	}
	
	/**
	 * Function used to download img form html page
	 * @param imgLink
	 * @param srcPath: output - duong dan de'n thuc muc de download hinh ve
	 */
	public static boolean downloadImg(String imgLink, String srcPath){
		boolean success = false;
		try {
			BufferedInputStream in;
			// encoding imgLink before openSteam- replace all blankSpace by %20
			imgLink = imgLink.replaceAll(" ", "%20");
			// open URL and download
			URL u = new URL(imgLink);
			URLConnection uc;
			uc = u.openConnection();
			uc.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
			uc.connect();
	        	in =  new BufferedInputStream(uc.getInputStream())  ;
	        
	        	imgLink=imgLink.replaceAll("http://", "");
	        	imgLink=imgLink.replaceAll("www.", "");
	        	// create an temporary outputPath and verifie is this file has already existed
	        	String outputPath = srcPath+imgLink;
			outputPath = outputPath.replaceAll("//", "/");
			File f = new File(outputPath);
			// if file has already existed, do nothing
			if (f.exists()){
				//System.out.println("File has already existed");
			}
			else { // else, download file
				String[] listDir = imgLink.split("/");
				for (int i = 0; i<listDir.length-1; i++){
					String dirName = listDir[i];
					srcPath += dirName + "/";
					//System.out.println("path = " +srcPath);
					f = new File(srcPath);
			        	f.mkdir();
				}
				srcPath += listDir[listDir.length-1];
				//System.out.println("path = " +srcPath);
		       		FileOutputStream fos = new FileOutputStream(srcPath);
				BufferedOutputStream bout = new BufferedOutputStream(fos,1024);
				byte[] data = new byte[1024];
				int x=0;
				while((x=in.read(data,0,1024))>=0){
					bout.write(data,0,x);
				}
				bout.close();
				success = true;
				//System.out.println("download successfull");
			}
			in.close();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.out.println("ERROR in downloadImg() function");
			e.printStackTrace();
			success = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("ERROR in downloadImg() function");
			e.printStackTrace();
			success = false;
		}
		return success;
	}
	
}
