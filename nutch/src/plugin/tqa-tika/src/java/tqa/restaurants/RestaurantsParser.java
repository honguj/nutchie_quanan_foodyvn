package tqa.restaurants;

import java.io.*;
import java.net.URL;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.parse.HtmlParseFilter;
import org.apache.nutch.protocol.*;
import org.apache.nutch.parse.*;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.tika.parser.ParseContext;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import org.w3c.dom.DocumentFragment;

public class RestaurantsParser implements HtmlParseFilter {
	private Configuration conf;

	public ParseResult filter(Content content, ParseResult parseResult, 
			HTMLMetaTags metaTags, DocumentFragment doc) {
		try {
			String link = content.getUrl();
			System.out.println("1)" + link);
		
			InputStream in = new FileInputStream("/all_nutch/nutchie_quanan_foodyvn/nutch/tqa-ListingFinder/ContentLinkOffline/com-que-dinh-liet.html");
		        
		        Metadata metadata = new Metadata();
			//creat metadata + ParseContext for tika parser
			HtmlParser parser = new HtmlParser();
			// create stream from url
			// InputStream in =  null ;

			// for parsing online
//			URL u = new URL(content.getUrl());
//			InputStream in = u.openStream();

			// for parsing offline
			//link = link.replace("file:", "");
//			System.out.println("RecommendedParser.java - link = " + link);
			//InputStream in = new FileInputStream(link);

			ParseContext context = new ParseContext();
			try {
				ContentHandler complexHandler = new CustomHandler(link);
				parser.parse(in,complexHandler,metadata,context);
				System.out.println("Metadata = " + metadata.size());
				String[] names = metadata.names();
				for (int i = 0 ; i < names.length; i++){
					System.out.println("meta: " + names[i] + " = " + metadata.get(names[i])) ;
				}
			} catch (SAXException e){ // parser.parse throw SAXException when Tika finished finding 5 fieds
				in.close();
				System.out.println(e.getMessage());
			}

		}
		catch (Exception e) {
			System.err.println("Error in RestaurantsParser.java: "+ e.getMessage());
			e.printStackTrace();
		}
		return parseResult;
	}

	public void setConf(Configuration conf) {
		this.conf = conf;
	}

	public Configuration getConf() {
		return this.conf;
	}

}
