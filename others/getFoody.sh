#!/bin/bash

# nutch directory
NUTCH_HOME="/all_nutch/nutchie_quanan_foodyvn/"

cd $NUTCH_HOME

# Create download folder if it doesnt exist
mkdir -p $NUTCH_HOME/downloadPages/contentLinks

# wget link listing and grep link contents
grep -o "http://www.foody.vn/[0-9a-zA-Z-]*/[0-9a-zA-Z-]*" foodyvn_sitemap.xml > seedList.txt

